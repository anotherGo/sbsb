# SBSB

Side By Side Binary Serialization
Ultra fast, zero allocation and simple serialization for network wired connections.
only supports (u)int(8/16/32/64) in big endian format, []byte and UTF-8 strings types.