package v1

import "errors"

var ErrNotPtr = errors.New("not a ptr")
var ErrNotStructPtr = errors.New("not a struct ptr")
var ErrIntUint = errors.New("can not use int and uint without size")
var ErrSliceFirst = errors.New("[]byte can not be first")
var ErrSideBySideSlices = errors.New("slices can not be side by side")
var ErrSliceAfterSigned = errors.New("slices can not be after a signed int")
var ErrUnsupportedSlice = errors.New("unsupported slice type")
var ErrUnsupportedType = errors.New("unsupported type")
var ErrNotStruct = errors.New("not a struct")
var ErrVersionNotSupproted = errors.New("version not supported")
