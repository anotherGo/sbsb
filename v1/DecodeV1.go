package v1

import "encoding/binary"
import "io"
import "reflect"
import "io/ioutil"

func Decode(target io.Reader, s interface{}) (n int, e error) {
	version := make([]byte, versionSize)
	n, e = target.Read(version)
	if e != nil {
		return
	}
	e = checkVersion(version)
	if e != nil {
		return
	}
	if reflect.TypeOf(s).Kind() != reflect.Ptr {
		e = ErrNotPtr
	}
	if reflect.TypeOf(s).Elem().Kind() != reflect.Struct {
		e = ErrNotStructPtr
	}
	var n2 int
	sValue := reflect.ValueOf(s).Elem()
	numFields := sValue.NumField()
	var field reflect.Value
	var preField reflect.Value
	for i := 0; i < numFields; i++ {
		field = sValue.Field(i)
		switch field.Kind() {
		case reflect.Int, reflect.Uint:
			n2 = 0
			e = ErrIntUint
		case reflect.Uint8:
			b := make([]byte, 1)
			n2, e = target.Read(b)
			field.SetUint(uint64(b[0]))
		case reflect.Int8:
			b := make([]byte, 1)
			n2, e = target.Read(b)
			field.SetInt(int64(b[0]))
		case reflect.Uint16:
			b := make([]byte, 2)
			n2, e = target.Read(b)
			field.SetUint(uint64(binary.BigEndian.Uint16(b)))
		case reflect.Int16:
			b := make([]byte, 2)
			n2, e = target.Read(b)
			field.SetInt(int64(binary.BigEndian.Uint16(b)))
		case reflect.Uint32:
			b := make([]byte, 4)
			n2, e = target.Read(b)
			field.SetUint(uint64(binary.BigEndian.Uint32(b)))
		case reflect.Int32:
			b := make([]byte, 4)
			n2, e = target.Read(b)
			field.SetInt(int64(binary.BigEndian.Uint32(b)))
		case reflect.Uint64:
			b := make([]byte, 8)
			n2, e = target.Read(b)
			field.SetUint(uint64(binary.BigEndian.Uint64(b)))
		case reflect.Int64:
			b := make([]byte, 8)
			n2, e = target.Read(b)
			field.SetInt(int64(binary.BigEndian.Uint64(b)))
		case reflect.String:
			if i < 1 {
				n2 = 0
				e = ErrSliceFirst
				break
			}
			if i == (numFields - 1) {
				var b []byte
				b, e = ioutil.ReadAll(target)
				if e == nil {
					field.SetString(string(b))
				}
				n2 = len(b)
				break
			}
			preField = sValue.Field(i - 1)
			b := make([]byte, preField.Uint())
			n2, e = target.Read(b)
			if e == nil {
				field.SetString(string(b))
			}
		case reflect.Slice:
			switch field.Type().Elem().Kind() {
			case reflect.Uint8:
				if i < 1 {
					n2 = 0
					e = ErrSliceFirst
					break
				}
				if i == (numFields - 1) {
					var b []byte
					b, e = ioutil.ReadAll(target)
					if e == nil {
						field.SetBytes(b)
					}
					n2 = len(b)
					break
				}
				preField = sValue.Field(i - 1)
				switch preField.Kind() {
				case reflect.Slice:
					n2 = 0
					e = ErrSideBySideSlices
					break
				case reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
					n2 = 0
					e = ErrSliceAfterSigned
					break
				}
				b := make([]byte, preField.Uint())
				n2, e = target.Read(b)
				if e == nil {
					field.SetBytes(b)
				}

			default:
				n2 = 0
				e = ErrUnsupportedSlice
			}
		default:
			n2 = 0
			e = ErrUnsupportedType
		}
		if n2 > 0 {
			n += n2
		}
		if e != nil {
			return
		}
	}
	return
}
