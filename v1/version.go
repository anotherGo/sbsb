package v1

var supportedVersions = [][]byte{[]byte{10}}
var DefaultVersion = 0

const versionSize = 1

func checkVersion(b []byte) error {
	if b[0] == supportedVersions[0][0] {
		return nil
	}
	return ErrVersionNotSupproted
}
