package v1

import "bytes"

func Unmarshal(b []byte, v interface{}) error {
	buff := bytes.NewReader(b)
	_, e := Decode(buff, v)
	return e
}
func Marshal(v interface{}) ([]byte, error) {
	buff := &bytes.Buffer{}
	_, e := Encode(buff, v)
	return buff.Bytes(), e
}
