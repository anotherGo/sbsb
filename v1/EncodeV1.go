package v1

import "encoding/binary"
import "io"
import "reflect"

func Encode(target io.Writer, s interface{}) (n int, e error) {
	n, e = target.Write(supportedVersions[DefaultVersion])
	if e != nil {
		return
	}
	if reflect.TypeOf(s).Name() != "struct" {
		e = ErrNotStruct
	}
	var n2 int
	var sValue reflect.Value = reflect.ValueOf(s)
	var numFields int = sValue.NumField()
	var field reflect.Value
	var nextField reflect.Value
	for i := 0; i < numFields; i++ {
		field = sValue.Field(i)
		switch field.Kind() {
		case reflect.Int, reflect.Uint:
			n2 = 0
			e = ErrIntUint
		case reflect.Uint8:
			fieldValue := field.Int()
			if fieldValue == 0 {
				nextField = sValue.Field(i + 1)
				switch nextField.Kind() {
				case reflect.Slice, reflect.String:
					fieldValue = int64(nextField.Len())
				}
			}
			n2, e = target.Write([]byte{uint8(fieldValue)})
		case reflect.Int8:
			fieldValue := field.Int()
			if fieldValue == 0 {
				nextField = sValue.Field(i + 1)
				switch nextField.Kind() {
				case reflect.Slice, reflect.String:
					fieldValue = int64(nextField.Len())
				}
			}
			n2, e = target.Write([]byte{uint8(fieldValue)})
		case reflect.Uint16:
			fieldValue := field.Int()
			if fieldValue == 0 {
				nextField = sValue.Field(i + 1)
				switch nextField.Kind() {
				case reflect.Slice, reflect.String:
					fieldValue = int64(nextField.Len())
				}
			}
			b := make([]byte, 2)
			binary.BigEndian.PutUint16(b, uint16(fieldValue))
			n2, e = target.Write(b)
		case reflect.Int16:
			fieldValue := field.Int()
			if fieldValue == 0 {
				nextField = sValue.Field(i + 1)
				switch nextField.Kind() {
				case reflect.Slice, reflect.String:
					fieldValue = int64(nextField.Len())
				}
			}
			b := make([]byte, 2)
			binary.BigEndian.PutUint16(b, uint16(fieldValue))
			n2, e = target.Write(b)
		case reflect.Uint32:
			fieldValue := field.Int()
			if fieldValue == 0 {
				nextField = sValue.Field(i + 1)
				switch nextField.Kind() {
				case reflect.Slice, reflect.String:
					fieldValue = int64(nextField.Len())
				}
			}
			b := make([]byte, 4)
			binary.BigEndian.PutUint32(b, uint32(fieldValue))
			n2, e = target.Write(b)
		case reflect.Int32:
			fieldValue := field.Int()
			if fieldValue == 0 {
				nextField = sValue.Field(i + 1)
				switch nextField.Kind() {
				case reflect.Slice, reflect.String:
					fieldValue = int64(nextField.Len())
				}
			}
			b := make([]byte, 4)
			binary.BigEndian.PutUint32(b, uint32(fieldValue))
			n2, e = target.Write(b)
		case reflect.Uint64:
			fieldValue := field.Int()
			if fieldValue == 0 {
				nextField = sValue.Field(i + 1)
				switch nextField.Kind() {
				case reflect.Slice, reflect.String:
					fieldValue = int64(nextField.Len())
				}
			}
			b := make([]byte, 8)
			binary.BigEndian.PutUint64(b, uint64(fieldValue))
			n2, e = target.Write(b)
		case reflect.Int64:
			fieldValue := field.Int()
			if fieldValue == 0 {
				nextField = sValue.Field(i + 1)
				switch nextField.Kind() {
				case reflect.Slice, reflect.String:
					fieldValue = int64(nextField.Len())
				}
			}
			b := make([]byte, 8)
			binary.BigEndian.PutUint64(b, uint64(fieldValue))
			n2, e = target.Write(b)
		case reflect.Slice:
			switch field.Type().Elem().Kind() {
			case reflect.Uint8:
				n2, e = target.Write(field.Bytes())
			default:
				n2 = 0
				e = ErrUnsupportedSlice
			}
		case reflect.String:
			n2, e = target.Write([]byte(field.String()))
		default:
			n2 = 0
			e = ErrUnsupportedType
		}
		if n2 > 0 {
			n += n2
		}
		if e != nil {
			return
		}
	}
	return
}
