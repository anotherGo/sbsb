package main

import "fmt"
import "bytes"
import ".."

type st struct {
	A     uint8
	Buff  []byte
	B     int16
	Buff2 []byte
}

func main() {
	s := st{
		A:     0,
		Buff:  []byte{1, 2, 3, 4, 5, 6},
		B:     12,
		Buff2: []byte{23, 2, 2, 4, 3, 234, 2, 34, 234, 23, 4, 23},
	}
	buff := &bytes.Buffer{}
	_, e := sbsb.Encode(buff, s)
	if e != nil {
		panic(e)
	}
	fmt.Println(buff.Bytes())
	s2 := st{}
	_, e = sbsb.Decode(buff, &s2)
	if e != nil {
		panic(e)
	}
	fmt.Println(s2)

}
