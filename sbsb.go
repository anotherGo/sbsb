package sbsb

import V1 "gitea.com/abgr/sbsb/v1"

var Encode = V1.Encode
var Decode = V1.Decode
var Marshal = V1.Marshal
var Unmarshal = V1.Unmarshal
